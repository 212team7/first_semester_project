* A: See parameters  
    * Show: Temperature range  
    * Show: water amount and interval  
    * D: Back  
* B: Set parameters  
    * A: Temperature range (xC - yC)  
        * NUMBER1 - # to accept  
        * NUMBER2 - # to accept  
        * D: Return to Set parameters  
    * B: Water amount and interval (xL - yh)  
        * NUMBER1 - # to accept  
        * NUMBER2 - # to accept  
        * D: Return to Set parameters  
    * C: Select predefined parameters  
        * 1-9: Predefined configuration x  
        * D: Back
    * D: Back  
* C: Show sensor readings  
    * Tempereature, humidity, soil humidity  
* D: Manual refresh of readings  
<br>
### Potential additions  
User can:
* Set data read interval
* Toggle data logging