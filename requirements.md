# Requirements
 **FURPS Requirements** | **M** | **S** | **C** | **W**
 :---:|:---:|:---:|:---:|:---:
 **Functionality** | **Must** | **Should** | **Could** | **Won't**
| | Battery | Watering mechanism | Window control | 
| | Microcontroller | Soil moisture sensor |  | 
| | Temperature sensor
| | Humidity sensor
**Usability** | **Must** | **Should** | **Could** | **Won't**
| | Display | Save user defined settings | Wireless Connectivity |
| | Status LED(s) | Predefined parameters | Data logging |
| | Buttons | Local data storage | |
**Reliability** | **Must** | **Should** | **Could** | **Won't**
| | Reduce power consumption | | |
| | Protection against weather | Protection against water | Backup Sensors | 
**Performance** | **Must** | **Should** | **Could** | **Won't**
| | Sense min. pr. 30 minutes | | |
**Supportability** | **Must** | **Should** | **Could** | **Won't**
