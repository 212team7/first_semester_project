# USECASE
## Initial Use Case

Use Case            | System Initiation
|---|---|
Goal                | The system saves the users requested parameters and starts operating
Actor               | User
Basic Flow          | The user selects which parameter they wanted changed and saved. It is visually shown with a screen, and is navigated using buttons.
Precondition        | The parameters must be configurated correctly
Postcondition       | The system is correctly configurated and starts operating.
Alternative flow    | If the user inputs an invalid value an error message is displayed, and the user must type in new value.
(Alternative flow)  | If bluetooth is implemented, the user can change parameters from an app via their phone or PC.

<br>

Use Case            | Auto adjustment
|-------------------|-------------------------------------------------|
Goal                | Adjust the environment to comply with the user parameters.
Actor               | Pico
Basic Flow          | The pico will read the sensors and adjust the windows to meet the user's set parameters. 
Assumption          | System Initiation
Alternative flow    | The input from the sensor is the as the user parameters. 

<br>

Use Case            | Basic flow: system initialisation
|-------------------|-------------------------------------------------|
Goal                | The user select the parameters and can switch between parameter
Actor               | User
1                   | User press the A,B,C or D button to navigate the menu.
2                   | A is pressed 

<br>

Use Case            | Auto adjustment
|-------------------|-------------------------------------------------|
Goal                | Adjust the environment to comply with the user parameters.
Actor               | Pico
Basic Flow          | The pico will read the sensors and adjust the windows to meet the user's set parameters. 
Assumption          | System Initiation
Alternative flow    | The input from the sensor is the as the user parameters. 



To water some plants and keep the temperature stable.

 The user inputs a wanted temperature and how often and how much to water the plants. The sensors reads temperature and air humidity inside and outside the greenhouse . Also read soil humidity inside. Compares the input from sensors to 