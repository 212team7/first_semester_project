# Requirements - MoSCoW
**Must** | **Should** | **Could** | **Won't**
:---:|:---:|:---:|:---:
| Battery | Watering mechanism | Window control | 
| Microcontroller | Soil moisture sensor | Backup Sensors |
| Temperature sensor | Save user defined settings | Wireless Connectivity |
| Humidity sensor | Predefined parameters | Data logging |
| Display | Local data storage
| Status LED(s)
| Buttons
