* Required Hardware
    * Microprocessor
        * Pico
    * battery
        * min. ~3v
    * water pump/valve
    * display
        * SSD1306
    * keypad
        * ZRX-543
    * 2x temperature sensor
        * BME-280
    * 2x humidity sensor
        * BME-280
    * soil moisture sensor
    * indicator LED
