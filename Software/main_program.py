from machine import Pin, PWM, I2C, ADC, RTC
import BME280 as bme280
import lowpower
from read_voltage import *
from save_dict_to_file import *
import ssd1306
import urtc
from menu_library import Menu_Manager
from keypad_library import Keypad
from timer import Custom_Timer
from utime import mktime, localtime, sleep

#Init peripherals and vars
DATA_READING_INTERVAL = 5
RTC_IRQ_PIN = 13
MIN_BATTERY_VOLTAGE = 2.15
PARAM_TEMP = 'Temperature'
PARAM_HUMID = 'Humidity'
PARAM_MOIST = 'Moisture'
PARAM_LIMITS = {PARAM_TEMP:(1,99),PARAM_HUMID:(1,99),PARAM_MOIST:(1,99)}
param_values = load_file()
if param_values == None:
    param_values = {PARAM_TEMP:[0,0],PARAM_HUMID:[0,0],PARAM_MOIST:[0,0]}

wake_up_pin = Pin(RTC_IRQ_PIN)
i2c_rtc = I2C(1,scl=Pin(11), sda=Pin(10), freq=100000)
rtc = urtc.DS3231(i2c_rtc)
rtc.datetime(urtc.seconds2tuple(mktime(localtime())))#Syncs RTCs
pico_rtc = RTC()

soil_sensor = machine.ADC(27)
pump = PWM(Pin(15))
pump.freq(512)
i2c = I2C(0, scl=Pin(17), sda=Pin(16))
bme = bme280.BME280(i2c=i2c,address=0x77)
bme.values

display_power = Pin(14, Pin.OUT, value = 1)
LINE_SPACING = 8
PIXEL_WIDTH = 128
display = ssd1306.SSD1306_I2C(PIXEL_WIDTH, 64, i2c)

def show_message(text, center = True):
    x = 0
    for i, line in enumerate(text):
        line = str(line)
        y = (len(menus_nav.current_menu.entries) + i) * LINE_SPACING
        text_length = len(line) * 8
        if center:
            remaining_space = PIXEL_WIDTH - text_length
            if remaining_space > 0:
                x = int(remaining_space/2)
        display.text(line,x,y)
    display.show()

def is_params_valid():
    for param in param_values:
        limits = PARAM_LIMITS[param]
        _range = range(limits[0], limits[1]+1)
        values = param_values[param]
        if values[0] not in _range or values[1] not in _range or values[0] >= values[1]:
            return False
    return True

def keypad_timeout(timer):
    keypad_timer.is_enabled = False
    if keypad.key_down_cb != keypad_callback:
        keypad.key_down_cb = keypad_callback
    if is_params_valid():
        take_reading()
    else:
        dormant()
keypad_timer = Custom_Timer(10000, keypad_timeout)

def low_battery_timeout(timer):
    dormant()
low_battery_timer = Custom_Timer(10000, low_battery_timeout)

led = Pin(25, Pin.OUT, value = 0)#Testing

saved_reading = load_file('reading_and_time.json')
if saved_reading != None:
    time_last_reading = urtc.datetime_tuple(*saved_reading['time'])
    last_reading = saved_reading['reading']
else:
    time_last_reading = urtc.seconds2tuple(urtc.tuple2seconds(rtc.datetime()) - DATA_READING_INTERVAL)
    last_reading = None

def print_entries(menu, trigger_entry = None):
    if menu.name == 'see_parameters':
        for i, param in enumerate(param_values):
            values = param_values[param]
            menu.entries[i].text = f"{param}:{values[0]}-{values[1]}"
    elif menu.name == 'set_parameter' and trigger_entry != None:
        menus_nav.set_menu(set_parameter_menu)
        menu.entries[0].text = trigger_entry.text
        _values = param_values[trigger_entry.text]
        menu.entries[1].text += str(_values[0])
        menu.entries[2].text += str(_values[1])
    display.fill(0)
    for index, entry in enumerate(menu.entries):
        if entry.activator:
            display.text(entry.activator + ':' + entry.text,0,index*LINE_SPACING)
        else:
            display.text(entry.text,0,index*LINE_SPACING)
    display.show()
    
def keypad_callback_set_param(key):
    num_str = ''
    if key.isdigit():
        keypad_timer.reset()
        num_str += key
        menus_nav.latest_trigger_entry.text += key
        print_entries(menus_nav.current_menu)
    else:
        keypad.key_down_cb = keypad_callback
        keypad_callback(key)

def keypad_callback(key):
    keypad_timer.reset()
    menus_nav.give_input(key)

menus_nav = Menu_Manager(print_entries)
keypad = Keypad(keypad_callback)

def save_parameter(entry, menus_nav):
    val_min, val_max= 0, 0
    for _entry in menus_nav.current_menu.entries:
        try:
            if 'Min' in _entry.text:
                val_min = int(_entry.text.split(':')[1])
            if 'Max' in _entry.text:
                val_max = int(_entry.text.split(':')[1])
        except ValueError or IndexError:
            show_message(['Value not set'])
            return
    if val_min >= val_max:
        show_message(['MIN must be','less than MAX'])
        return
    _param = menus_nav.current_menu.entries[0].text
    _limits = PARAM_LIMITS[_param]
    _range = range(_limits[0], _limits[1]+1)
    if val_min not in _range or val_max not in _range:
        show_message(['Allowed range:', f'{_limits[0]} - {_limits[1]}'])
        return
    #SUCCESS
    param_values[_param] = [val_min, val_max]
    save_to_file(param_values)
    show_message(['VALUES SAVED'])

def init_set_parameter(entry, menus_nav):
    entry.text = entry.text.split(':')[0] + ':'
    keypad.key_down_cb = keypad_callback_set_param
    print_entries(menus_nav.current_menu)
    
def show_latest_reading(entry, menus_nav):
    print_entries(menus_nav.current_menu)
    show_message(last_reading)
    
def menu_take_reading(entry, menus_nav):
    take_reading(False, True)
    print_entries(menus_nav.current_menu)
    show_message(last_reading)

def set_test(entry, menus_nav):#TODO
    print('set test')

def testing(interval=0.5):
    utime.sleep(interval)
    led.on()
    utime.sleep(interval)
    led.off()
    
def seconds_since_last_reading():
    then = urtc.tuple2seconds(time_last_reading)
    now = urtc.tuple2seconds(rtc.datetime())
    return now - then
    
def remap(value, maxInput, minInput, maxOutput, minOutput):
    value = maxInput if value > maxInput else value
    value = minInput if value < minInput else value
    inputSpan = maxInput - minInput
    outputSpan = maxOutput - minOutput
    scaledThrust = float(value - minInput) / float(inputSpan)
    return minOutput + (scaledThrust * outputSpan)

def take_reading(go_dormant = True, force = False):
    _seconds = seconds_since_last_reading()
    if _seconds >= DATA_READING_INTERVAL or force:
        raw_soil_reading = soil_sensor.read_u16()
        soil = remap(raw_soil_reading, 45000, 400, 100, 1)
        (temp, _pressure, humid) = bme.values
        global time_last_reading, last_reading
        time_last_reading = rtc.datetime()
        last_reading = (temp, humid, soil)
        _tuple = urtc.datetimetuple2tuple(time_last_reading)
        dict_to_save = {'reading':last_reading,'time':_tuple}
        save_to_file(dict_to_save,'reading_and_time.json')
        if last_reading[-1] < 15:
            pump.duty_u16(40000)
            sleep(2)
            pump.duty_u16(0)
    if go_dormant:
        if _seconds >= DATA_READING_INTERVAL or _seconds <= 5:
            dormant(DATA_READING_INTERVAL)
        else:
            dormant(DATA_READING_INTERVAL - _seconds)

def wakeup_init():
    display_power.on()
    keypad.col_pins[-3].irq(None)
    wake_up_pin.irq(None)
    [pin.low() for pin in keypad.row_pins]
    while True:
        try:
            global display
            display = ssd1306.SSD1306_I2C(PIXEL_WIDTH, 64, i2c)
            break
        except:
            continue

def wakeup_keypad(_pin):
    wakeup_init()
    if is_params_valid():
        print_entries(menus_nav.get_menu('main_menu'))
        keypad_timer.start()
        
def wakeup_rtc(_pin):
    wakeup_init()

def dormant(duration = 0):
    [pin.high() for pin in keypad.row_pins]
    keypad.col_pins[-3].irq(wakeup_keypad, trigger = Pin.IRQ_RISING)
    display_power.off()
    if duration <= 0:
        lowpower.dormant_until_pin(keypad.cols[-3])
    elif duration > 0:
        rtc.alarm(False) #Enable RTC alarm
        rtc.interrupt() #Enable RTC alarm interrupt
        alarm_date = urtc.seconds2tuple(urtc.tuple2seconds(rtc.datetime()) + duration)
        alarm_date = urtc.datetime_tuple(year=alarm_date.year, month=alarm_date.month, day=alarm_date.day, weekday=None,#Sets weekday to None because
                                hour=alarm_date.hour, minute=alarm_date.minute, second=alarm_date.second, millisecond=0)#alarm_time() raises error when both weekday and day is set
        rtc.alarm_time(alarm_date) #Set datetime of RTC alarm to x seconds in the future
        wake_up_pin.irq(wakeup_rtc, trigger = Pin.IRQ_FALLING)
        lowpower.dormant_with_modes({
            RTC_IRQ_PIN:lowpower.LEVEL_LOW,
            keypad.cols[-3]:lowpower.LEVEL_HIGH,
        })
    #---------AFTER DORMANT---------#
        rtc.no_interrupt() #Disable RTC alarm interrupt
    pico_rtc.datetime(urtc.datetimetuple2tuple(rtc.datetime())) #Syncs RTCs

set_parameter_menu = ['set_parameter',
    ['', 'PARAM NAME', ''],
    ['A', 'Min:', init_set_parameter],
    ['B', 'Max:', init_set_parameter],
    ['C', 'Save', save_parameter],
    ['D', 'Back', 'set_parameters']
    ]

menus = [  
    ['main_menu',
    ['A', 'See Parameters', 'see_parameters'],
    ['B', 'Set Parameters', 'set_parameters'],
    ['C', 'Latest reading', show_latest_reading],
    ['D', 'Take reading', menu_take_reading]
    ],
    ['see_parameters',
    ['', PARAM_TEMP, ''],
    ['', PARAM_HUMID, ''],
    ['', PARAM_MOIST, ''],
    ['D', 'Back', 'main_menu']
    ],
    ['set_parameters',
    ['A', PARAM_TEMP, 'set_parameter'],
    ['B', PARAM_HUMID, 'set_parameter'],
    ['C', PARAM_MOIST, 'set_parameter'],
    ['#', 'Default Configs', 'predefined_config1'],
    ['D', 'Back', 'main_menu']
    ],
    #Predfined configs: Dictionaries with a nested tuples with value range for each parameter
    #{'Carrots':({PARAM_TEMP:(15,20),PARAM_HMID:(30,80),PARAM_MOIST:(20,80)})}
    #set_test() looks up the values via entry.text TODO
    ['predefined_config2',
    ['5', 'Carrots', set_test],
    ['6', 'Tomatoes', set_test],
    ['7', 'Bell peber', set_test],
    ['8', 'bla bla', set_test],
    ['C', 'Page1', 'predefined_config1'],
    ['D', 'Back', 'set_parameters']
    ],
    ['predefined_config1',
    ['1', 'bla bla', set_test],
    ['2', 'bla bla', set_test],
    ['3', 'bla bla', set_test],
    ['4', 'bla bla', set_test],
    ['C', 'Page2', 'predefined_config2'],
    ['D', 'Back', 'set_parameters']
    ]
]
menus_nav.add_menus(menus)
menus_nav.add_menu(set_parameter_menu)

while True:
    try:
        if read_voltage() > MIN_BATTERY_VOLTAGE:
            if keypad_timer.is_enabled:
                keypad.check_for_event()
            elif is_params_valid():
                take_reading()
            else:
                print_entries(menus_nav.get_menu('main_menu'))
                show_message([' ','INVALID PARAMS'])
                keypad_timer.start()
        else:
            show_message(['LOW BATTERY'])
            low_battery_timer.start()
    except Exception as e:
        f = open('error_log.txt', 'a')
        f.write(str(e)+'\n')
        f.close()