import deep_sleep_GPIO_IRQ as lowpower
from machine import Pin
from time import sleep

DORMANT_PIN = 15

led = Pin(25, Pin.OUT, value = 0)
btn = Pin(DORMANT_PIN, Pin.IN, Pin.PULL_UP)
btn.irq(led.value(1), trigger = Pin.IRQ_RISING)

led.value(1)
print("before dormant")
sleep(2)
led.value(0)
lowpower.dormant_until_pin(DORMANT_PIN)