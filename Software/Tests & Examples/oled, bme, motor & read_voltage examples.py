from machine import Pin, I2C,ADC,PWM
import ssd1306
import BME280 as bme280
from time import sleep
from read_voltage import read_voltage

led_onboard = PWM(Pin(25))
led_onboard.freq(100)
led_onboard.duty_u16(int(65535/10))

i2c = I2C(0,scl=Pin(1),sda=Pin(0))
display = ssd1306.SSD1306_I2C(128, 64, i2c)
bme = bme280.BME280(i2c=i2c, address=0x77)
bme1 = bme280.BME280(i2c=i2c, address=0x76)

motor = PWM(Pin(2))

def reMap(value, maxInput, minInput, maxOutput, minOutput):
    value = maxInput if value > maxInput else value
    value = minInput if value < minInput else value

    inputSpan = maxInput - minInput
    outputSpan = maxOutput - minOutput

    scaledThrust = float(value - minInput) / float(inputSpan)

    return minOutput + (scaledThrust * outputSpan)

def scroll_in_screen(screen):
  for i in range (128+1, -len(screen[0][2])*8, -1):
    for line in screen:
        if line[3]:
            display.text(line[2], i, line[1])
        else:
            display.text(line[2], line[0], line[1])
    display.show()
    if i!= 128:
      display.fill(0)

while read_voltage(28) > 2.15:
    voltage=str(read_voltage(28))+'V'
    
    line0=[0,0,bme.values[0]+" "+bme.values[2]+" "+bme.values[1],True]
    line1=[0,20,bme1.values[0]+" "+bme1.values[2]+" "+bme1.values[1],True]
    line2=[128-40,64-10,voltage,False]
    screen=[line0,line1,line2]
    scroll_in_screen(screen)

    motor.freq(50)
    motor.duty_u16(0)

    sleep(0.5)
