import urtc
import utime
import lowpower
import BME280 as bme280
import ssd1306
from save_dict_to_file import *
from machine import Pin, I2C, RTC

def log(data,file='log.txt'):
    with open(file, 'a') as f:
        f.write(data)
        f.close()

i2c_rtc = I2C(1,scl=Pin(11), sda=Pin(10), freq=100000)
ipin=Pin(13)
rtc = urtc.DS3231(i2c_rtc)
test_rtc=RTC()
rtc.datetime(urtc.seconds2tuple(utime.mktime(utime.localtime())))
rtc.interrupt() #Enable alarm interrupt

i2c = I2C(0, scl=Pin(17), sda=Pin(16))
bme = bme280.BME280(i2c=i2c,address=0x77)
bme.values

display_power = Pin(14, Pin.OUT, value = 1)
LINE_SPACING = 8
PIXEL_WIDTH = 128
display = ssd1306.SSD1306_I2C(PIXEL_WIDTH, 64, i2c)

INTERVAL = 5
time_last_reading = rtc.datetime()

led = Pin(25, Pin.OUT)

def wakeup(pin = None):
    display_power.on()
    while True:
        try:
            global display
            display = ssd1306.SSD1306_I2C(PIXEL_WIDTH, 64, i2c)
            break
        except:
            continue
ipin.irq(wakeup, trigger = Pin.IRQ_FALLING)

def test(duration=0.5):
    utime.sleep(duration)
    led.on()
    utime.sleep(duration)
    led.off()
    
def dormant():
    #test()
    display_power.off()
    alarm_date = urtc.seconds2tuple(urtc.tuple2seconds(rtc.datetime())+INTERVAL)
    alarm_date = urtc.datetime_tuple(year=alarm_date.year, month=alarm_date.month, day=alarm_date.day, weekday=None,
                                hour=alarm_date.hour, minute=alarm_date.minute, second=alarm_date.second, millisecond=0)
    log(f'Set alarm: {alarm_date}\n')
    rtc.alarm_time(alarm_date) #Set datetime of DS3231 alarm to x seconds later
    rtc.alarm(False) #Set alarm flags in STATUS_REGISTER to 0
    lowpower.dormant_with_modes({13:lowpower.LEVEL_LOW,})
    
    _datetime = rtc.datetime()
    _datetime = (_datetime.year, _datetime.month, _datetime.day, _datetime.weekday,
        _datetime.hour, _datetime.minute, _datetime.second, 0)
    test_rtc.datetime(_datetime)

    display.fill(0)
    display.text('Testing...',0,0)
    display.show()
    test()

def take_reading():
    test(0.25)
    global time_last_reading
    time_last_reading = rtc.datetime()
    last_reading = bme.values
    _tuple = urtc.datetimetuple2tuple(time_last_reading)
    dict_to_save = {'reading':last_reading,'time':_tuple}
    save_to_file(dict_to_save,'reading_and_time.json')
    test(0.25)
    
def seconds_since_last_reading():
    global time_last_reading
    now = urtc.tuple2seconds(rtc.datetime())
    then = urtc.tuple2seconds(time_last_reading)
    return now - then

#take_reading()
#asdasd
reading = load_file('reading_and_time.json')
test1 = reading['time']
test2 = reading['reading']
print(test1,'\n',test2)
rtc.datetime(urtc.datetime_tuple(*test1))
print(rtc.datetime())
asdasd
while True:
    try:
        if seconds_since_last_reading() >= INTERVAL:
            take_reading()
        dormant()
    except Exception as e:
        log(str(e)+'\n')