from time import sleep

# CONSTANTS
#KEY_UP = 0
IS_PRESSED = 1

keys = [['1', '2', '3', 'A'], ['4', '5', '6', 'B'], ['7', '8', '9', 'C'], ['*', '0', '#', 'D']]

####################################
# Example use of menu library
####################################
def print_entries(menu, trigger_entry = None):
    if menu.name == 'set_parameter' and trigger_entry != None:
            menu.entries[0].text = trigger_entry.text
    for entry in menu.entries:
        print(entry.activator + ': ' + entry.text)

from menu_library import Menu_Manager
menus_nav = Menu_Manager(print_entries)
temp_min, temp_max = 0, 0
humid_min, humid_max = 0, 0
TEMP_MIN, TEMP_MAX = 0, 99
HUMID_MIN, HUMID_MAX = 0, 99

def test(entry, menus_nav):
    print('test')

def set_test(entry, menus_nav):
    print('set test')

def save_parameter(entry, menus_nav):
    val_min, val_max= -1, -1
    for _entry in menus_nav.current_menu.entries:
        try:
            if 'Min' in _entry.text:
                val_min = int(_entry.text.split()[1])
            if 'Max' in _entry.text:
                val_max = int(_entry.text.split()[1])
        except IndexError:
            print('One of the values is not set')
            return
    if val_min >= val_max:
        print('MIN must be less than MAX')
        return
    if menus_nav.current_menu.entries[0].text == 'Temperature':
        if val_min >= TEMP_MIN and val_max <= TEMP_MAX:
            global temp_min, temp_max
            temp_min = val_min
            temp_max = val_max
        else:
            print('Allowed range:', TEMP_MIN, '-', TEMP_MAX)
            return
    elif menus_nav.current_menu.entries[0].text == 'Humidity':
        if val_min >= HUMID_MIN and val_max <= HUMID_MAX:
            global humid_min, humid_max
            humid_min = val_min
            humid_max = val_max
        else:
            print('Allowed range:', HUMID_MIN, '-', HUMID_MAX)
            return
    #SUCCESS
    print(temp_min, temp_max)
    print(humid_min, humid_max)
    

def init_set_parameter(entry, menus_nav):
    num_str = ''
    entry.text = entry.text.split()[0] + " "
    while True:
        key = input()
        if key.isdigit():
            num_str += key
            entry.text += key
            print_entries(menus_nav.current_menu)
        else:
            if entry.activator == key:
                entry.text = entry.text.split()[0] + " "
            break
    menus_nav.give_input(key)
    menus_nav.set_menu(set_parameter_menu)


set_parameter_menu = ['set_parameter',
    ['', 'TITLE', ''],
    ['A', 'Min: ', init_set_parameter],
    ['B', 'Max: ', init_set_parameter],
    ['C', 'Save', save_parameter],
    ['D', 'Back', 'set_parameters']
    ]

menus = [
    ###MENU FORMAT###
    # [MENU_NAME, [ENTRY_ACTIVATOR, ENTRY_TEXT, ENTRY_ACTION], [NEXT ENTRY], ...]
    # ENTRY_ACTION can be a name of another menu or a function to call
    
    ['main_menu',
    ['A', 'See Parameters', 'see_parameters'],
    ['B', 'Set Parameters', 'set_parameters']
    ],

    ['see_parameters',
    ['A', 'Show Temperature', test],
    ['B', 'Show Water', test],
    ['D', 'Back', 'main_menu']
    ],

    ['set_parameters',
    ['A', 'Temperature', 'set_parameter'],
    ['B', 'Humidity', 'set_parameter'],
    ['C', 'Predefined config', 'predefined_config1'],
    ['D', 'Back', 'main_menu']
    ],

    ['predefined_config2',
    ['5', '50-60, ur mamma', set_test],
    ['6', '60-70, ur mamma', set_test],
    ['7', '70-80, ur mamma', set_test],
    ['8', '80-90, ur mamma', set_test],
    ['9', '90-99, ur mamma', set_test],
    ['C', 'Page1', 'predefined_config1'],
    ['D', 'Back', 'set_parameters']
    ],

    ['predefined_config1',
    ['0', '0-10, ur mamma', set_test],
    ['1', '10-20, ur mamma', set_test],
    ['2', '20-30, ur mamma', set_test],
    ['3', '30-40, ur mamma', set_test],
    ['4', '40-50, ur mamma', set_test],
    ['C', 'Page2', 'predefined_config2'],
    ['D', 'Back', 'set_parameters']
    ]
]

menus_nav.add_menus(menus)
menus_nav.add_menu(set_parameter_menu)

print_entries(menus_nav.current_menu)

while True:
    key = input()
    menus_nav.give_input(key)
                

'''
* C: Show sensor readings  
    * Tempereature, humidity, soil humidity  
* D: Manual refresh of readings  
'''