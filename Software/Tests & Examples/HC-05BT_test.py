from machine import Pin, UART
import utime
uart0 = UART(0,9600, bits=8, parity=None, stop=1, tx=Pin(12), rx=Pin(13))
#uart0 = UART(0,38400, bits=8, parity=None, stop=1, tx=Pin(12), rx=Pin(13))
at_tgl = Pin(27,Pin.OUT,value=0)
pwr_tgl = Pin(28,Pin.OUT,value=0)
connect_status = Pin(0,Pin.IN,Pin.PULL_DOWN)

def sendCMD_waitResp(cmd, uart=uart0, timeout=5000):
    print("CMD: " + cmd)
    uart.write(cmd)
    waitResp(uart, timeout)
    
def waitResp(uart=uart0, timeout=5000):
    prvMills = utime.ticks_ms()
    resp = b""
    while (utime.ticks_ms()-prvMills)<timeout:
        if uart.any():
            resp = b"".join([resp, uart.read(1)])
    resp=resp.decode('utf-8').rstrip()
    print(resp)

while True:
    print(connect_status.value())
    utime.sleep(10)
    

#sendCMD_waitResp('AT+RMAAD\r\n')
#sendCMD_waitResp('AT+ROLE=0\r\n')
#sendCMD_waitResp('AT+CMODE=0\r\n')
#sendCMD_waitResp('AT+ADDR?\r\n')
