# demo 1

from machine import Pin
from time import sleep

# CONSTANTS
KEY_UP   = const(0)
KEY_DOWN = const(1)

keys = [['1', '2', '3', 'A'], ['4', '5', '6', 'B'], ['7', '8', '9', 'C'], ['*', '0', '#', 'D']]

# Pin names for Pico
rows = [2,3,4,5]
cols = [6,7,8,9]

# set pins for rows as outputs
row_pins = [Pin(pin_name, mode=Pin.OUT) for pin_name in rows]

# set pins for cols as inputs
col_pins = [Pin(pin_name, mode=Pin.IN, pull=Pin.PULL_DOWN) for pin_name in cols]

def init():
    for row in range(0,4):
        for col in range(0,4):
            row_pins[row].low()

def scan(row, col):
    """ scan the keypad """

    # set the current column to high
    row_pins[row].high()
    key = None

    # check for keypressed events
    if col_pins[col].value() == KEY_DOWN:
        key = KEY_DOWN
    if col_pins[col].value() == KEY_UP:
        key = KEY_UP
    row_pins[row].low()

    # return the key state
    return key

def return_menu():
    global key_press
    global menu_state
    if key == KEY_DOWN:
        key_press = keys[row][col]
        if key_press == "D":
            menu_state = state[0]
            print(menu_state)
            sleep(0.5)
                    
state = ["no_menu", "see_parameters", "set_parameters", "show_sensor_readings", "manual_refresh", "set_range", "set_hum_range", "set_water_range"]

print("starting")

# set all the columns to low
init()

key_press = None
menu_state = state[0]

while True:
    if menu_state == state[0]:
        for row in range(4):
            for col in range(4):
                key = scan(row, col)
                if key == KEY_DOWN:
                    key_press = keys[row][col]
                    if key_press == "A":
                        menu_state = state[1]
                        print(menu_state)
                        print(" *temperature range* \n *humidity range* \n *water amount/interval*")
                    elif key_press == "B":
                        menu_state = state[2]
                        print(menu_state)
                        print(" press A to set temperature range \n press B to set humidity range \n press C to set water amount")
                    elif key_press == "C":
                        menu_state = state[3]
                        print(menu_state)
                        print(" [PLEASE HOLDER TEXT] \n Temperature: FUCK ITS HOT IN HERE \n Humidity: Im moist!")
                    elif key_press == "D":
                        print(" refresh")
                    sleep(0.5)
    # see parameters
    if menu_state == state[1]:
        for row in range(4):
            for col in range(4):
                key = scan(row, col)
                return_menu()
    # set parameters
    if menu_state == state[2]:
        for row in range(4):
            for col in range(4):
                key = scan(row, col)
                return_menu()
    # show sensor readings
    if menu_state == state[3]:
        for row in range(4):
            for col in range(4):
                key = scan(row, col)
                return_menu()
     