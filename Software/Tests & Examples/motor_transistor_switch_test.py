from machine import Pin,PWM
from time import sleep

MAX_DUTY_CYCLE = 65535

motor = PWM(Pin(15))
motor.freq(100)
motor.duty_u16(MAX_DUTY_CYCLE)

while True:
    duty_cycle_p = int(input("Input 0-100: "))
    if duty_cycle_p >= 0 and duty_cycle_p <= 100:
        duty_cycle = int(MAX_DUTY_CYCLE / 100 * duty_cycle_p)
        motor.duty_u16(duty_cycle)
    else:
        continue