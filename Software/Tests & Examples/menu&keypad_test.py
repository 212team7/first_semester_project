from machine import Pin, I2C
from main_program import PARAM_MOIST
from menu_library import Menu_Manager
from keypad_library import Keypad
import ssd1306
#import lowpower
display_power = Pin(14, Pin.OUT, value = 1)
from time import sleep
sleep(0.05)
i2c = I2C(0,scl=Pin(17),sda=Pin(16))
#for device in i2c.scan():
#    print('Adress',device,hex(device))

PIXEL_WIDTH = 128
try:
    display = ssd1306.SSD1306_I2C(PIXEL_WIDTH, 64, i2c)
except Exception as e:
    print(e)

####################################
# Keypad GPIO IRQs testing
####################################
# def test(pin):
#     print('event')
#     global led2
#     led2.on()
#     sleep(1)
#     led2.off()

led2 = Pin(25, Pin.OUT, value = 0)
#led = Pin(1, Pin.OUT, value = 1)
# col_pins[-1].irq(test, trigger = Pin.IRQ_RISING)
# [pin.high() for pin in row_pins]
# lowpower.dormant_until_pin(9)
# led = Pin(1, Pin.OUT, value = 0)
# sleep(0.5)
# led.on()
# sleep(0.5)
# led.off()

PARAM_TEMP = 'Temperature'
PARAM_HUMID = 'Humidity'
PARAM_LIMITS = {PARAM_TEMP:(1,99),PARAM_HUMID:(1,99)}
param_values = {PARAM_TEMP:[0,0],PARAM_HUMID:[0,0]}

LINE_SPACING = 8

def print_entries(menu, trigger_entry = None):
    if menu.name == 'see_parameters':
        _temp = param_values[PARAM_TEMP]
        _humid = param_values[PARAM_HUMID]
        menu.entries[0].text = f"Temp:{_temp[0]}-{_temp[1]}"
        menu.entries[1].text = f"Humid:{_humid[0]}-{_humid[1]}"
    elif menu.name == 'set_parameter' and trigger_entry != None:
        menus_nav.set_menu(set_parameter_menu)
        menu.entries[0].text = trigger_entry.text
        _values = param_values[trigger_entry.text]
        menu.entries[1].text += str(_values[0])
        menu.entries[2].text += str(_values[1])
    display.fill(0)
    for index, entry in enumerate(menu.entries):
        display.text(entry.activator + ':' + entry.text,0,index*LINE_SPACING)
    display.show()

menus_nav = Menu_Manager(print_entries)
keypad = Keypad(menus_nav.give_input)

#Menu instead of function. Similar to see_parameters menu?
def show_latest_reading(entry, menus_nav):
    print('No way')
    
def take_reading(entry, menus_nav):
    print('Forget it')

def test(entry, menus_nav):
    print('test')

def set_test(entry, menus_nav):
    print('set test')
    
def show_message(text, center = True):
    x = 0
    for i, line in enumerate(text):
        y = (len(menus_nav.current_menu.entries) + i) * LINE_SPACING
        text_length = len(line) * 8
        if center:
            remaining_space = PIXEL_WIDTH - text_length
            if remaining_space > 0:
                x = int(remaining_space/2)
        display.text(line,x,y)
        display.show()

def save_parameter(entry, menus_nav):
    val_min, val_max= 0, 0
    for _entry in menus_nav.current_menu.entries:
        try:
            if 'Min' in _entry.text:
                val_min = int(_entry.text.split(':')[1])
            if 'Max' in _entry.text:
                val_max = int(_entry.text.split(':')[1])
        except ValueError or IndexError:
            show_message(['Value not set'])
            return
    if val_min >= val_max:
        show_message(['MIN must be','less than MAX'])
        return
    _param = menus_nav.current_menu.entries[0].text
    _limits = PARAM_LIMITS[_param]
    _range = range(_limits[0], _limits[1]+1)
    if val_min not in _range or val_max not in _range:
        show_message(['Allowed range:', f'{_limits[0]} - {_limits[1]}'])
        return
    #SUCCESS
    global param_values
    param_values[_param] = [val_min, val_max]
    show_message(['VALUES SAVED'])
    
def keypad_cb_set_param(key):
    num_str = ''
    if key.isdigit():
        num_str += key
        menus_nav.latest_trigger_entry.text += key
        print_entries(menus_nav.current_menu)
    else:
        keypad.key_down_cb = menus_nav.give_input
        menus_nav.give_input(key)

def init_set_parameter(entry, menus_nav):
    entry.text = entry.text.split(':')[0] + ':'
    keypad.key_down_cb = keypad_cb_set_param
    print_entries(menus_nav.current_menu)

set_parameter_menu = ['set_parameter',
    ['', 'TITLE', ''],
    ['A', 'Min:', init_set_parameter],
    ['B', 'Max:', init_set_parameter],
    ['C', 'Save', save_parameter],
    ['D', 'Back', 'set_parameters']
    ]

menus = [
    ###MENU FORMAT###
    # [MENU_NAME, [ENTRY_ACTIVATOR, ENTRY_TEXT, ENTRY_ACTION] , [ENTRY] , ...]
    # ENTRY_ACTION can be a name of another menu or a function to call   
    ['main_menu',
    ['A', 'See Parameters', 'see_parameters'],
    ['B', 'Set Parameters', 'set_parameters'],
    ['C', 'Latest reading', show_latest_reading],
    ['D', 'Take reading', take_reading]
    ],

    ['see_parameters',
    ['', f"Temp:{param_values[PARAM_TEMP][0]}-{param_values[PARAM_TEMP][1]}", ''],
    ['', f"Humid:{param_values[PARAM_HUMID][0]}-{param_values[PARAM_HUMID][1]}", ''],
    ['', f"Moist:{param_values[PARAM_MOIST][0]}-{param_values[PARAM_MOIST][1]}", ''],
    ['D', 'Back', 'main_menu']
    ],

    ['set_parameters',
    ['A', PARAM_TEMP, 'set_parameter'],
    ['B', PARAM_HUMID, 'set_parameter'],
    ['B', PARAM_MOIST, 'set_parameter'],
    ['C', 'Defalt Configs', 'predefined_config1'],
    ['D', 'Back', 'main_menu']
    ],

    #Predfined configs: Dictionaries with a nested tuples with value range for each parameter
    #{'Carrots':((15,20),(30,80))}
    #set_test() looks up the values via entry.text
    ['predefined_config2',
    ['5', 'Carrots', set_test],
    ['6', 'Tomatoes', set_test],
    ['7', 'Bell peber', set_test],
    ['8', 'bla bla', set_test],
    ['C', 'Page1', 'predefined_config1'],
    ['D', 'Back', 'set_parameters']
    ],

    ['predefined_config1',
    ['1', 'bla bla', set_test],
    ['2', 'bla bla', set_test],
    ['3', 'bla bla', set_test],
    ['4', 'bla bla', set_test],
    ['C', 'Page2', 'predefined_config2'],
    ['D', 'Back', 'set_parameters']
    ]
]

menus_nav.add_menus(menus)
menus_nav.add_menu(set_parameter_menu)

print_entries(menus_nav.get_menu('main_menu'))

while True:
    keypad.check_for_event()
            