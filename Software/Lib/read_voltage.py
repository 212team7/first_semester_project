from machine import ADC,Pin
#from read_voltage import read_voltage
def read_voltage():
    REPETITIONS = 10
    average = 0
    for i in range(REPETITIONS):
        try:
            #print(ADC(29).read_u16())
            average += round(ADC(29).read_u16()*(3.26 / (65535)) * 3,2)
        except:
            return "ERR"
    return average / REPETITIONS
