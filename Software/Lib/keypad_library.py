from machine import Pin

class Keypad:
    def __init__(self, key_down_callback = None, key_up_callback = None):
        self.KEY_UP = 0
        self.KEY_DOWN = 1

        self.key_up_cb = key_up_callback
        self.key_down_cb = key_down_callback
        self.currently_pressed_key = None #Allows for single key press. Triggers only one key down and key up event

        self.keys = [['1', '2', '3', 'A'], ['4', '5', '6', 'B'], ['7', '8', '9', 'C'], ['*', '0', '#', 'D']]

        # GPIO pin assignments
        self.rows = [2,3,4,5]
        self.cols = [6,7,8,9]

        # Set pins for rows as outputs
        self.row_pins = [Pin(pin_name, mode=Pin.OUT, value = 0) for pin_name in self.rows]

        # Set pins for columns as inputs
        self.col_pins = [Pin(pin_name, mode=Pin.IN, pull=Pin.PULL_DOWN) for pin_name in self.cols]

    def check_for_event(self):
        for row in range(4):
            for col in range(4):
                self.row_pins[row].high()
                # Check for key pressed events
                if self.col_pins[col].value() == self.KEY_DOWN and self.currently_pressed_key == None:
                    if self.key_down_cb != None:
                        self.key_down_cb(self.keys[row][col])
                    self.currently_pressed_key = self.keys[row][col]
                if self.currently_pressed_key != None:
                    if self.col_pins[col].value() == self.KEY_UP and self.keys[row][col] == self.currently_pressed_key:
                        if self.key_up_cb != None:
                            self.key_up_cb(self.currently_pressed_key)
                        self.currently_pressed_key = None
                self.row_pins[row].low()
