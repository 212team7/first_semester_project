from machine import Timer

class Custom_Timer:
    #Timer.ONE_SHOT = 0
    #Timer.PERIODIC = 1
    def __init__(self, period, callback, mode = Timer.ONE_SHOT):
        self.period = period
        self.mode = mode
        self.callback = callback
        self._timer = Timer()
        self.is_enabled = False
        
    def start(self):
        self.is_enabled = True
        self._timer.init(mode=self.mode, period=self.period, callback=self.callback)
    
    def stop(self):
        self.is_enabled = False
        self._timer.deinit()
    
    def reset(self):
        self.stop()
        self.start()