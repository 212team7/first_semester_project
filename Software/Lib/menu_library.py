class Menu_Manager:
    def __init__(self, menu_changed_callback_func, menus = []):
        self.current_menu = None
        self.menus = menus
        if not menus:
            self.add_menus(menus)
        self.callback = menu_changed_callback_func
        self.latest_trigger_entry = None

    def add_menu(self, menu):
        if type(menu) is list:
            self.menus.append(self.Menu(menu[0], menu[1:]))
        else:
            self.menus.append(menu)
        if len(self.menus) == 1:
            self.current_menu = self.menus[0]
        return self.menus[-1]
    
    def add_menus(self, menus):
        for menu in menus:
            self.add_menu(menu)

    def get_menu(self, menu_name):
        for menu in self.menus:
            if menu.name == menu_name:
                return menu
        return None

    def get_menu_index(self, menu_name):
        for i, menu in enumerate(self.menus):
            if menu.name == menu_name:
                return i
        return None

    def set_menu(self, new_menu):
        if type(new_menu) is list:
            menu_name = new_menu[0]
            new_menu = self.Menu(menu_name, new_menu[1:])
        else: menu_name = new_menu.name
        i = self.get_menu_index(menu_name)
        if i != None:
            self.menus[i] = new_menu

    def give_input(self, activator):
        if activator:
            for entry in self.current_menu.entries:
                if entry.activator == activator:
                    self.latest_trigger_entry = entry
                    if type(entry.action) is str:
                        self.current_menu = self.get_menu(entry.action)
                        self.callback(self.current_menu, entry)
                    else:
                        entry.action(entry, self)
    
    class Menu:
        def __init__(self, name, menu):
            self.name = name
            self.entries = []
            for entry in menu:
                self.entries.append(self.Menu_Entry(*entry))

        class Menu_Entry:
            def __init__(self, activator, text, action):
                self.activator = activator
                self.text = text
                self.action = action