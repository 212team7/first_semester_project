import json

def save_to_file(data: dict,file:str='save_file.json'):
    with open(file, 'w') as f:
        f.write(json.dumps(data))
        f.close()
                
def load_file(file:str='save_file.json'):
    try:
        with open(file) as f:
            return json.loads(f.read())
    except:
        return None